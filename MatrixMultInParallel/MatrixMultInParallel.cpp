// MatrixMultInParallel.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <fstream>
#include <string>
#include <chrono>
#include <omp.h>

#define DEFAULT_SIZE 500
using namespace std;

void writeMatrix(string path, int ** matrix);
void showMatrix(int ** matrix);
void matrixMultiplication(int** a, int** b, int** c);
void initMatrix(int **m, int a);

int main()
{
	int ** a = new int*[DEFAULT_SIZE];
	int ** b = new int*[DEFAULT_SIZE];
	int ** c = new int*[DEFAULT_SIZE];
	for (int i = 0; i < DEFAULT_SIZE; ++i) {
		a[i] = new int[DEFAULT_SIZE];
		b[i] = new int[DEFAULT_SIZE];
		c[i] = new int[DEFAULT_SIZE];
	}

	initMatrix(a, 10);
	initMatrix(b, 1);
	initMatrix(c, 0);

	std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
	matrixMultiplication(a, b, c);
	std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
	std::cout << "Time difference = " << std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count() << std::endl;
	writeMatrix("res.txt", c);
	system("pause");
	return 0;
}

void writeMatrix(string path, int ** matrix) {
	int x, y;
	ofstream of(path);

	if (!of) {
		cout << "Cannot open file.\n";
		return;
	}

	for (y = 0; y < DEFAULT_SIZE; y++) {
		for (x = 0; x < DEFAULT_SIZE; x++) {
			of << matrix[x][y]<<" ";
		}
		of << endl;
	}

	of.close();
}

void showMatrix(int ** matrix) {
	for (int i = 0; i < DEFAULT_SIZE; ++i) {
		for (int j = 0; j < DEFAULT_SIZE; ++j) {
			cout << matrix[i][j] << "\t";
		}
		cout << endl;
	}
}

void matrixMultiplication(int** a, int** b, int** c) {
	for (int i = 0; i<DEFAULT_SIZE; i++) {
		for (int j = 0; j<DEFAULT_SIZE; j++)
			for (int k = 0; k<DEFAULT_SIZE; k++)
				c[i][j] += a[i][k] * b[k][j];
	}
}

void initMatrix(int **m, int value) {
	for (int i = 0; i < DEFAULT_SIZE; ++i) {
		for (int j = 0; j < DEFAULT_SIZE; ++j) {
			m[i][j] = value;
		}
	}
}

